﻿using System;

namespace BattleshipGameCore
{
	/// <summary>
	/// position on Battleship game board
	/// X, Y are zero based horizontal and vertical indexes
	/// </summary>
	public sealed class Position
	{
		/// <summary>
		/// keep in sync with MaxPos
		/// </summary>
		private const string MaxPosString = "57";

		/// <summary>
		/// maximum allowed value for X and Y, chosen for simplicity to represent columns with one letter 'A' .. 'z'
		/// more columns may be represented with more letters
		/// </summary>
		public const int MaxPos = 57;

		/// <summary>
		/// zero based horizontal component
		/// </summary>
		public readonly int X;

		/// <summary>
		/// zero based vertical component
		/// </summary>
		public readonly int Y;

		public Position(int x, int y)
		{
			const string OUT_OF_RANGE_MESSAGE = "must be between 0 and " + MaxPosString + " inclusive";

			if(x < 0 || MaxPos < x)
				throw new ArgumentOutOfRangeException(nameof(x), OUT_OF_RANGE_MESSAGE);
			if(y < 0 || MaxPos < y)
				throw new ArgumentOutOfRangeException(nameof(y), OUT_OF_RANGE_MESSAGE);

			X = x;
			Y = y;
		}

		/// <summary>
		/// 'A' .. 'z' case sensitive
		/// </summary>
		public char Column => (char)('A' + X);

		/// <summary>
		/// one based (starts from one)
		/// </summary>
		public int Row => 1 + Y;
	}
}
