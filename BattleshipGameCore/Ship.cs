﻿using System;

namespace BattleshipGameCore
{
	/// <summary>
	/// Battleship game ship
	/// </summary>
	public sealed class Ship
	{
		// ship may need some sort of Key/ID to identify if saved/loaded or exchanged between processes/systems
		// this is not required - ships are identified by their object identity

		public readonly Position StartPosition;
		public readonly int Length;
		public readonly bool IsVertical;

		public Ship(Position startPosition, int length, bool isVertical)
		{
			if(startPosition == null)
				throw new ArgumentNullException(nameof(startPosition));
			if(length < 1)
				throw new ArgumentOutOfRangeException(nameof(length), "must be > 0");

			StartPosition = startPosition;
			Length = length;
			IsVertical = isVertical;
		}
	}
}
