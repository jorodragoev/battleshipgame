﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BattleshipGameCore
{
	public enum TryAddShipResult
	{
		Success,

		/// <summary>
		/// cannot add - length is greater than board dimension
		/// </summary>
		TooLong,

		/// <summary>
		/// chosen start position, length and orientation do not entirely fit on board
		/// </summary>
		DoesNotFit,

		/// <summary>
		/// cannot add - will overlap another ship already placed on board
		/// </summary>
		Overlaps,

		/// <summary>
		/// same ship was previously placed
		/// </summary>
		AlreadyPlaced,

		/// <summary>
		/// not absolutely necessary but assume required: "During setup ...", "During play ..."
		/// </summary>
		GameAlreadyStarted

		/// <summary>
		/// allowed number of ships with that length is already placed on board or ship with that length is not allowed
		/// normally the game has such a rule
		/// I assume this is not required: "During setup, players can place an arbitrary number of “battleships”"
		/// </summary>
		//ExceedsLimit
	}

	public enum StrikeStatus
	{
		/// <summary>
		/// invalid position
		/// </summary>
		NotOnBoard = -1,

		/// <summary>
		/// position was already tried
		/// </summary>
		AlreadyStruck,

		/// <summary>
		/// ok - did not hit any ship
		/// </summary>
		Miss,

		/// <summary>
		/// ok - hit a ship but it has more unhit fields
		/// </summary>
		Hit,

		/// <summary>
		/// ok - hit the last fields of a ship - there are more ships
		/// </summary>
		SunkShip,

		/// <summary>
		/// ok - hit the last fields of the last ship - game over
		/// </summary>
		SunkAllShips
	}

	public readonly struct StrikeResult
	{
		public readonly StrikeStatus Status;
		public readonly Ship? Ship;

		public StrikeResult(StrikeStatus status, Ship? ship)
		{
			if(status > StrikeStatus.Hit ^ ship != null)
				throw new ArgumentException("ship must be not null when status is SunkXXX, otherwise ship must be null");

			Status = status;
			Ship = ship;
		}
	}

	/// <summary>
	/// Battleship game board holding players ships, tracks and reports status
	/// modeled after https://www.mathsisfun.com/games/battleship.html
	/// </summary>
	public sealed class Board
	{
		/// <summary>
		/// keep in sync with MaxDim
		/// </summary>
		private const string MaxDimString = "58";

		/// <summary>
		/// maximum allowed value for the two dimensions
		/// </summary>
		public const int MaxDim = Position.MaxPos + 1;

		public readonly int DimX;
		public readonly int DimY;

		private bool m_hasGameStarted;

		/// <summary>
		/// row major matrix of fields on the board, true if struck
		/// </summary>
		private readonly bool[] m_fields;

		/// <summary>
		/// ships placed on board with corresopnding field indexes that each ship occupies
		/// when ship is sunk it is removed from here
		/// </summary>
		private readonly Dictionary<Ship, int[]> m_fieldIndexesByShip = new Dictionary<Ship, int[]>();

		/// <summary>
		/// creates a board with default dimensions: 10x10
		/// </summary>
		public Board() : this(10, 10)
		{
			//OK
		}

		/// <summary>
		/// creates a board with specified dimensions
		/// both must be > 0
		/// </summary>
		public Board(int dimX, int dimY)
		{
			const string OUT_OF_RANGE_MESSAGE = "must be between 1 and " + MaxDimString + " inclusive";

			if(dimX < 1 || MaxDim < dimX)
				throw new ArgumentOutOfRangeException(nameof(dimX), OUT_OF_RANGE_MESSAGE);
			if(dimY < 1 || MaxDim < dimY)
				throw new ArgumentOutOfRangeException(nameof(dimY), OUT_OF_RANGE_MESSAGE);

			DimX = dimX;
			DimY = dimY;

			m_fields = new bool[DimY * DimX];
		}

		private (TryAddShipResult result, int[]? aix) CheckCanAddShip(Position startPosition, int length, bool isVertical)
		{
			if(!isVertical && length > DimX || isVertical && length > DimY)
		return (TryAddShipResult.TooLong, null);

			if(!isVertical && (startPosition.X + length) > DimX || isVertical && (startPosition.Y + length) > DimY)
		return (TryAddShipResult.DoesNotFit, null);

			int[] aix = new int[length];

			if(!isVertical)
				for(int i = 0; i < length; i++)
					aix[i] = startPosition.Y * DimX + startPosition.X + i;
			else
				for(int i = 0; i < length; i++)
					aix[i] = (startPosition.Y + i) * DimX + startPosition.X;

			// if any new index is also an index for any of already placed ships
			if(m_fieldIndexesByShip.Any(item => aix.Intersect(item.Value).Any()))
				return (TryAddShipResult.Overlaps, null);
			else
				return (TryAddShipResult.Success, aix);
		}

		/// <summary>
		/// check specified parameters and if possible create and add ship on this board
		/// recommended primary way to place ships
		/// </summary>
		public TryAddShipResult TryCreateShipOnThisBoard(Position startPosition, int length, bool isVertical, out Ship? ship)
		{
			if(startPosition == null)
				throw new ArgumentNullException(nameof(startPosition));

			ship = null;

			if(m_hasGameStarted)
		return TryAddShipResult.GameAlreadyStarted;

			(
				TryAddShipResult result,
				int[]? aix
			)
				= CheckCanAddShip(startPosition, length, isVertical);

			if(result != TryAddShipResult.Success)
		return result;

			ship = new Ship(startPosition, length, isVertical);

			m_fieldIndexesByShip.Add(ship, aix!);

			return result;
		}

		/// <summary>
		/// tries to add ship on this board
		/// in case ship has been previosly removed but need to be placed again
		/// may be used as alternative to TryCreateShipOnThisBoard but may end up creating many ships that cannot be placed
		/// </summary>
		public TryAddShipResult TryAddShip(Ship ship)
		{
			if(ship == null)
				throw new ArgumentNullException(nameof(ship));

			if(m_hasGameStarted)
		return TryAddShipResult.GameAlreadyStarted;

			if(m_fieldIndexesByShip.ContainsKey(ship))
		return TryAddShipResult.AlreadyPlaced;

			(
				TryAddShipResult result,
				int[]? aix
			)
				= CheckCanAddShip(ship.StartPosition, ship.Length, ship.IsVertical);

			if(result != TryAddShipResult.Success)
		return result;

			m_fieldIndexesByShip.Add(ship, aix!);

			return TryAddShipResult.Success;
		}

		/// <summary>
		/// removes ship from this board if ship exists and game has not yet started
		/// </summary>
		/// <returns>true on success</returns>
		public bool RemoveShip(Ship ship) => !m_hasGameStarted && m_fieldIndexesByShip.Remove(ship);

		public StrikeResult Strike(Position position)
		{
			if(position == null)
				throw new ArgumentNullException(nameof(position));

			m_hasGameStarted = true;

			if(position.X > DimX - 1 || position.Y > DimY - 1)
		return new StrikeResult(StrikeStatus.NotOnBoard, null);

			int ix = position.Y * DimX + position.X;

			if(m_fields[ix])
		return new StrikeResult(StrikeStatus.AlreadyStruck, null);

			m_fields[ix] = true;

			// iteration is fine - default case is 10x10 - vast amount of ships and indexes to check is not expected
			KeyValuePair<Ship, int[]> shipFields = m_fieldIndexesByShip.FirstOrDefault(item => item.Value.Contains(ix));

			if(shipFields.Key == null)
		return new StrikeResult(StrikeStatus.Miss, null);

			// if any ship field is not yet struck
			if(shipFields.Value.Any(ix => !m_fields[ix]))
		return new StrikeResult(StrikeStatus.Hit, null);

			m_fieldIndexesByShip.Remove(shipFields.Key);

			if(m_fieldIndexesByShip.Count > 0)
				return new StrikeResult(StrikeStatus.SunkShip, shipFields.Key);
			else
				return new StrikeResult(StrikeStatus.SunkAllShips, shipFields.Key);
		}

		/// <summary>
		/// recommended usage is to call once, choose position to strike and remove that position
		/// removed position can be stored separately together with strike result
		/// may be called more than once
		/// </summary>
		/// <returns>unordered positions available for strike</returns>
		public List<Position> GetUnstruckPositions()
		{
			List<Position> result = new List<Position>();

			for(int j = 0; j < DimY; j++) // select row
				for(int i = 0; i < DimX; i++) // select column
					if(!m_fields[j * DimX + i])
						result.Add(new Position(i, j));

			return result;
		}

		/// <summary>
		/// ordered Column Keys
		/// </summary>
		public IEnumerable<char> ColumnKeys => Enumerable.Range(0, DimX).Select(item => (char)('A' + item));

		/// <summary>
		/// ordered Row Keys
		/// </summary>
		public IEnumerable<int> RowKeys => Enumerable.Range(0, DimY).Select(item => 1 + item);
	}
}
