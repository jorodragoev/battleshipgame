﻿using System;
using Xunit;

namespace BattleshipGameCore
{
	public class PositionTests
	{
		[Fact()]
		public void ValidCreatePositionTest()
		{
			Position result = new Position(Position.MaxPos, Position.MaxPos - 1);

			Assert.NotNull(result);
			Assert.Equal(Position.MaxPos, result.X);
			Assert.Equal('z', result.Column);
			Assert.Equal(Position.MaxPos - 1, result.Y);
			Assert.Equal(Position.MaxPos, result.Row);
		}

		[Fact()]
		public void InalidCreatePositionTest()
		{
			Assert.Throws<ArgumentOutOfRangeException>
			(
				() => new Position(-1, 0)
			);
			Assert.Throws<ArgumentOutOfRangeException>
			(
				() => new Position(0, -1)
			);
			Assert.Throws<ArgumentOutOfRangeException>
			(
				() => new Position(Position.MaxPos + 1, 0)
			);
			Assert.Throws<ArgumentOutOfRangeException>
			(
				() => new Position(0, Position.MaxPos + 1)
			);
		}
	}
}