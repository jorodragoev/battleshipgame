﻿using System;
using System.Collections.Generic;
using Xunit;

namespace BattleshipGameCore
{
	public class BoardTests
	{
		[Fact()]
		public void CreateDefaultBoardTest()
		{
			Board result = new Board();

			Assert.NotNull(result);
			Assert.Equal(10, result.DimX);
			Assert.Equal(10, result.DimY);
			Assert.Equal(new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'}, result.ColumnKeys);
			Assert.Equal(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }, result.RowKeys);
		}

		[Fact()]
		public void ValidCreateBoardTest()
		{
			Board result = new Board(2, 1);

			Assert.NotNull(result);
			Assert.Equal(2, result.DimX);
			Assert.Equal(1, result.DimY);
			Assert.Equal(new char[] { 'A', 'B' }, result.ColumnKeys);
			Assert.Equal(new int[] { 1 }, result.RowKeys);
		}

		[Fact()]
		public void InvalidCreateBoardTest()
		{
			Assert.Throws<ArgumentOutOfRangeException>
			(
				() => new Board(0, 1)
			);
			Assert.Throws<ArgumentOutOfRangeException>
			(
				() => new Board(1, 0)
			);
			Assert.Throws<ArgumentOutOfRangeException>
			(
				() => new Board(Board.MaxDim + 1, 1)
			);
			Assert.Throws<ArgumentOutOfRangeException>
			(
				() => new Board(1, Board.MaxDim + 1)
			);
		}

		[Fact()]
		public void TryCreateShipOnThisBoardTest()
		{
			//TODO:
		}

		[Fact()]
		public void TryAddShipTest()
		{
			//TODO:
		}

		[Fact()]
		public void RemoveShipTest()
		{
			//TODO:
		}

		[Fact()]
		public void StrikeTest()
		{
			//TODO:
		}

		[Fact()]
		public void GetUnstruckPositionsTest()
		{
			//TODO:
		}

		[Fact()]
		public void SimulationTest()
		{
			Random rnd = new Random();
			Board board = new Board(11, 9);

			List<Ship> ships = new List<Ship>()
			{
				CreateShip(5),
				CreateShip(4),
				CreateShip(3),
				CreateShip(3),
				CreateShip(2),
				CreateShip(2),
				CreateShip(1),
				CreateShip(1)
			};

			Assert.DoesNotContain(ships, ship => ship == null);

			List<Position> unstruckPositions = board.GetUnstruckPositions();

			Assert.NotNull(unstruckPositions);
			Assert.Equal(board.DimY * board.DimX, unstruckPositions.Count);

			StrikeResult result;

			do
			{
				int ix = rnd.Next(0, unstruckPositions.Count - 1);

				result = board.Strike(unstruckPositions[ix]);
				unstruckPositions.RemoveAt(ix);

				Assert.InRange(result.Status, StrikeStatus.Miss, StrikeStatus.SunkAllShips);

				if(result.Status == StrikeStatus.Miss || result.Status == StrikeStatus.Hit)
					Assert.Null(result.Ship);
				else
				{
					Assert.NotNull(result.Ship);

					int i = ships.IndexOf(result.Ship!);

					Assert.True(i >= 0);

					ships.RemoveAt(i);
				}
			}
			while(result.Status != StrikeStatus.SunkAllShips);

			Assert.Empty(ships);

			Ship CreateShip(int length)
			{
				Ship ship;
				bool isVertical = rnd.Next(2) != 0;

				while
				(
					board.TryCreateShipOnThisBoard
					(
						new Position
						(
							rnd.Next(board.DimX - (!isVertical ? length - 1 : 0)),
							rnd.Next(board.DimY - (isVertical ? length - 1 : 0))
						),
						length,
						isVertical,
						out ship!
					) != TryAddShipResult.Success
				)
					isVertical = rnd.Next(2) != 0;

				return ship!;
			}
		}
	}
}