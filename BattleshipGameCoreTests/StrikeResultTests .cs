﻿using System;
using Xunit;

namespace BattleshipGameCore
{
	public class StrikeResultTests
	{
		[Fact()]
		public void ValidCreateStrikeResultNoShipTest()
		{
			StrikeResult result = new StrikeResult(StrikeStatus.Hit, null);

			Assert.Equal(StrikeStatus.Hit, result.Status);
			Assert.Null(result.Ship);
		}

		[Fact()]
		public void ValidCreateStrikeResultWithShipTest()
		{
			Ship ship = new Ship(new Position(0, 0), 1, false);

			StrikeResult result = new StrikeResult(StrikeStatus.SunkShip, ship);

			Assert.Equal(StrikeStatus.SunkShip, result.Status);
			Assert.Equal(ship, result.Ship);
		}

		[Fact()]
		public void InvalidCreateStrikeResultNoShipTest()
		{
			Assert.Throws<ArgumentException>
			(
				() => new StrikeResult(StrikeStatus.SunkAllShips, null)
			);
		}

		[Fact()]
		public void InvalidCreateStrikeResultWithShipTest()
		{
			Ship ship = new Ship(new Position(0, 0), 1, false);

			Assert.Throws<ArgumentException>
			(
				() => new StrikeResult(StrikeStatus.Hit, ship)
			);
		}
	}
}