﻿using System;
using Xunit;

namespace BattleshipGameCore
{
	public class ShipTests
	{
		[Fact()]
		public void ValidCreateShipTest()
		{
			Position position = new Position(Position.MaxPos, Position.MaxPos);
			Ship result = new Ship(position, int.MaxValue, true);

			Assert.NotNull(result);
			Assert.Equal(position, result.StartPosition);
			Assert.Equal(int.MaxValue, result.Length);
			Assert.True(result.IsVertical);
		}

		[Fact()]
		public void InvalidCreateShipNullPositionTest()
		{
			Assert.Throws<ArgumentNullException>
			(
				() => new Ship(null!, 3, false)
			);
		}

		[Fact()]
		public void InvalidCreateShipZeroLengthTest()
		{
			Position position = new Position(0, 0);

			Assert.Throws<ArgumentOutOfRangeException>
			(
				() => new Ship(position, 0, false)
			);
		}
	}
}